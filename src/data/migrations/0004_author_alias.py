# Generated by Django 3.1.5 on 2021-01-25 15:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('data', '0003_auto_20210122_2053'),
    ]

    operations = [
        migrations.AddField(
            model_name='author',
            name='alias',
            field=models.CharField(default='', max_length=64),
        ),
    ]
