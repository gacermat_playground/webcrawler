from data.models import *


class DataToModelInjector():
    def injectData(self, statsData: list):
        
        self.__flushDataBase__()
        for entry in statsData:
            author = entry['author']
            authorAlias = entry['author_alias']
            wordList = entry['words']

            thisAuthorCount = Author.objects.filter(authorName = author).count()
            if thisAuthorCount is 0:
                newAuthor = Author(authorName = author, alias = authorAlias)
                newAuthor.save()

                for w in wordList:
                    newStat = Stats(word = w, count = wordList[w], author = newAuthor)
                    newStat.save()
            else:
                existingAuthor = Author.objects.get(authorName = author)
                for w in wordList:
                    newStat = Stats(word = w, count = wordList[w], author = existingAuthor)
                    newStat.save()
                    
    def __flushDataBase__(self):
        Author.objects.all().delete()
        Stats.objects.all().delete()