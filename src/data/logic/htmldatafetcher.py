import requests

import logging
logger = logging.getLogger(__name__)


class HtmlDataFetcher():
    def __init__(self):
        pass
    
    def getDocumentFromUrl(self, url):
        try:
            httpResponse = requests.get(url)
            logger.info("Response obtained from url: " 
                        + str(httpResponse.url) + " with status code: " + str(httpResponse.status_code))
            
            if httpResponse.status_code == 200:
                return httpResponse.content
            else:
                logger.error("Content corrupted, bad response")
                return ""
            
        except:
            logger.error("Error while requesting from: " + url)
            return ""