from bs4 import BeautifulSoup
import re
from string import punctuation
from collections import Counter
from unidecode import unidecode
from data.logic.htmldatafetcher import HtmlDataFetcher

import logging
logger = logging.getLogger(__name__)


class BlogHtmlStatsSerializer():
    def __init__(self, baseUrl: str, htmlFetcher: HtmlDataFetcher):
        self.baseUrl: str = baseUrl
        self.htmlFetcher: HtmlDataFetcher = htmlFetcher
        
    def getStatsCollection(self) -> list:
        rawContent = self.htmlFetcher.getDocumentFromUrl(self.baseUrl)
        htmlDocSoup = BeautifulSoup(rawContent, 'lxml') # lxml package must be installed in env
        postContainersList = htmlDocSoup.find_all('div', class_ = 'post-container')
        
        refLinkList = []
        for entry in postContainersList:
            postTitleTag = entry.find('h2', class_ = 'post-title')
            postRefLink: str = postTitleTag.find('a').get('href')
            refLinkList.append(postRefLink.replace('../blog/', '')) # get rid of initial link part
            
        pageNumbers = htmlDocSoup.find('span', class_ = 'page-number').text.split('/')[1]
        for p in range(2, int(pageNumbers) + 1):
            rawContent = self.htmlFetcher.getDocumentFromUrl(self.baseUrl + f"page/{str(p)}/index.html")
            htmlDocSoup = BeautifulSoup(rawContent, 'lxml')
            postContainersList = htmlDocSoup.find_all('div', class_ = 'post-container')
            
            for entry in postContainersList:
                postTitleTag = entry.find('h2', class_ = 'post-title')
                postRefLink: str = postTitleTag.find('a').get('href')
                refLinkList.append(postRefLink.replace('../../../blog/', ''))
          
        logger.info("Collected posts ref links:")
        logger.info(refLinkList)
        
        # TODO: obtain serialized data
        statsDictList = []
        for ref in refLinkList:
            statsDictList.append(self.__getStatsPerPost__(self.baseUrl, ref))
        
        return statsDictList
        
    def __getStatsPerPost__(self, baseUrl: str, postUrlTrail: str):
        postPage = self.htmlFetcher.getDocumentFromUrl(baseUrl + postUrlTrail)
        postSoup = BeautifulSoup(postPage, 'lxml')
        
        author: str = postSoup.find('span', class_ = 'author-name').get_text()
        postContent = postSoup.find('div', class_ = 'post-content')
        paragraphsText: str =  "".join(t.get_text() + ' ' for t in postContent.find_all('p'))
        headersText: str =  "".join(h.get_text()+' ' for h in postContent.find_all('h2'))
        
        # removing existing hyperlinks enclosed in '(' ')' and raw ones
        pattern = re.compile('[(]?http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.#&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+')
        paragraphsText = pattern.sub('', paragraphsText)

        unifiedText = paragraphsText + headersText
        wordList = unifiedText.split()
        
        wordListWithoutPunctuation = []
        for word in wordList:
            wordListWithoutPunctuation.append(word.strip(punctuation+'“'))
            
        wordOccurenceCollection = Counter(wordListWithoutPunctuation)
        
        statsDict = {}
        statsDict['author'] = author
        statsDict['author_alias'] = unidecode(author.replace(' ', '').lower())
        statsDict['words'] = dict(wordOccurenceCollection)

        return statsDict