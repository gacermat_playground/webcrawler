from threading import Thread, Lock

# Meta class within Singleton design
class DataCollectorInterfaceMeta(type):
    _instances = {}
    _locker = Lock()
    
    def __instance_check__(cls, instance):
        return cls.__subclasscheck__(type(instance))
    
    def __subclasscheck(cls, subclass):
        return (hasattr(subclass, 'collect')) and callable(subclass.collect)
    
    def __call__(cls, *args, **kwargs):
        with cls._locker:
            if cls not in cls._instances:
                instance = super().__call__(*args, **kwargs)
                cls._instances[cls] = instance
        return cls._instances[cls]
        
        
class DataCollectorInterface(metaclass = DataCollectorInterfaceMeta):
    # Dummy interface class
    pass
    