from data.logic.datacollectorinterface import DataCollectorInterface
from data.models import *
import time
from threading import Thread, Lock
from data.logic.htmldatafetcher import HtmlDataFetcher
from data.logic.bloghtmlstatsserializer import BlogHtmlStatsSerializer
from data.logic.datatomodelinjector import DataToModelInjector

import logging
logger = logging.getLogger(__name__)

UPDATE_PERIOD = 60 # seconds
URL_PLACEHOLDER = "https://teonite.com/blog/"


class DataCollectorThreadedWrapper(DataCollectorInterface):
    def __init__(self):
        self.thr = None
        self.running = False
        self.lock = Lock()
        self.dataFetcher = HtmlDataFetcher()
    
    def collect(self):
        # in case of invoking while data processing is in progress: blocking approach
        self.lock.acquire()
        self.__runFetching__()
        self.lock.release()
    
    def __runFetching__(self):  
        if self.thr is None:
            # first run blocking fetching
            self.__fetch__()
            
            logger.info("Launching periodic data fetching in singleton thread")
            self.thr = Thread(target=self.__threadedMethod__)
            self.running = True
            self.thr.start()
        else:
            logger.info("Update thread is already running... ommiting on-call fetching")
            pass
    
    def __fetch__(self):
        htmlStatsSerializer = BlogHtmlStatsSerializer(URL_PLACEHOLDER, self.dataFetcher)
        serializedStats = htmlStatsSerializer.getStatsCollection()
        
        # comment-out for optimization
        # logger.info(serializedStats)
        
        dataInjector = DataToModelInjector()
        dataInjector.injectData(serializedStats)   
            
    def __threadedMethod__(self):
        while self.running:
            time.sleep(UPDATE_PERIOD)
            self.lock.acquire()
            
            logger.info("Obtaining data from external source (HTTP request) via periodic update")
            self.__fetch__()
            
            self.lock.release()