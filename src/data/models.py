from django.db import models

class Author(models.Model):
    authorName = models.CharField(max_length=64, default='')
    alias = models.CharField(max_length=64, default='')
    

class Stats(models.Model):
    word = models.CharField(max_length=64, default='')
    count = models.IntegerField()
    author = models.ForeignKey(Author, null=True, related_name='poster', on_delete=models.SET_NULL)
