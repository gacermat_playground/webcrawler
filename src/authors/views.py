from django.shortcuts import render
from django.db.models import Model
from rest_framework.response import Response
from rest_framework import viewsets

from authors.serializers import *
from data.models import Author

import logging
logger = logging.getLogger(__name__)

from data.logic.datacollectorthreadedwrapper import *


class ViewSetAuthors(viewsets.ModelViewSet):
    serializer_class = AuthorsNamesSerializer
    
    def get_queryset(self):
        dataCollector = DataCollectorThreadedWrapper()
        dataCollector.collect()
        return Author.objects.all().order_by('authorName')
        
    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())   
        page = self.paginate_queryset(queryset)
    
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, many=True)
    
        mergedDict = {}
        tempDict = {}
        for item in serializer.data:
            tempDict[item['alias']] = item['authorName']
            mergedDict = {**mergedDict, **tempDict}
            
        return Response(mergedDict)