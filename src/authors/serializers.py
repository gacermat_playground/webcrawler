from data.models import Author
from rest_framework import serializers


class AuthorsNamesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = ("alias", "authorName")
