from data.models import Author
from data.models import Stats
from rest_framework import serializers

class ByAuthorStatsSerializer(serializers.ModelSerializer):
    
    totalWords = serializers.IntegerField()
    
    class Meta:
        model = Stats
        fields = ("word", "totalWords")


class StatsSerializer(serializers.ModelSerializer):       

    totalWords = serializers.IntegerField()

    class Meta:
        model = Stats
        fields = ("word", "totalWords")
        