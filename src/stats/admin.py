from django.contrib import admin
from data.models import Author
from data.models import Stats

admin.site.register(Author)
admin.site.register(Stats)
