from django.urls import path, include
from rest_framework import routers
from .views import *

router = routers.DefaultRouter()
router.register(prefix="(?P<author>\D+)", viewset=ViewSetByAuthor, basename="viewByAuthor")
router.register("", viewset=ViewSetStats, basename="viewStats")

urlpatterns = router.urls
