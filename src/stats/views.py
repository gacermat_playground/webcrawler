from django.shortcuts import render
from django.db.models import Count
from django.db.models import Sum
from django.db.models import Model
from rest_framework.response import Response
from rest_framework import viewsets

from stats.serializers import *
from data.models import Author
from data.models import Stats

import logging
logger = logging.getLogger(__name__)

from data.logic.datacollectorthreadedwrapper import *

NUM_OF_MOST_COMMON_WORDS = 10

class ViewSetBase(viewsets.ModelViewSet):
    def __composeResponseData__(self, valAsKey, valOfKey):
        queryset = self.filter_queryset(self.get_queryset())   
        page = self.paginate_queryset(queryset)

        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, many=True)

        mergedDict = {}
        tempDict = {}
        for item in serializer.data:
            tempDict[item[valAsKey]] = item[valOfKey]
            mergedDict = {**mergedDict, **tempDict}

        return mergedDict


class ViewSetByAuthor(ViewSetBase):
    serializer_class = ByAuthorStatsSerializer
    
    def get_queryset(self):
        dataCollector = DataCollectorThreadedWrapper()
        dataCollector.collect()
        
        aAlias = self.kwargs['author']     
        if aAlias:
            authorObj = Author.objects.all().get(alias__exact=aAlias)
            return (Stats.objects.values('word').filter(author_id__exact=authorObj.id).annotate(totalWords=Sum('count'))).order_by('-totalWords')[:NUM_OF_MOST_COMMON_WORDS]
        else:
            return {"Error": "Unresolved Error"} # Placeholder
    
    def list(self, request, *args, **kwargs):
        return Response(self.__composeResponseData__('word', 'totalWords'))
    
    
class ViewSetStats(ViewSetBase):    
    serializer_class = StatsSerializer
    
    def get_queryset(self):
        dataCollector = DataCollectorThreadedWrapper()
        dataCollector.collect()
        return (Stats.objects.values('word').annotate(totalWords=Sum('count'))).order_by('-totalWords')[:NUM_OF_MOST_COMMON_WORDS]
    
    
    def list(self, request, *args, **kwargs):
        return Response(self.__composeResponseData__('word', 'totalWords'))